//@ npm module dependecy =======
let express = require("express");
let app = express();
let bodyParser = require("body-parser");

//@ config file
let config = require("./server/helpers/config")();

//@ api service  route ======//
let user = require("./server/routes/userRoute");

//@ create database connection =====//
let connectWithDatabase = require("./server/dbConnection/dao")

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json({
    limit: "50mb"
}));
app.use(bodyParser.urlencoded({
    limit: "50mb"
    , extended: true
    , parameterLimit: 50000
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ("OPTIONS" === req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});

//@apply the routes to our application with the prefix /api
app.use("/user", user);

// start the server =========
let listener = app.listen(config.PORT, function (err, success) {
    console.log("Listening on port-->> " + listener.address().port);
});







// 1. http: //localhost:8084/user/registration | POST  
//     Request: {
//         "name": "sushee"
//         , "phoneNumber": 1234567867
//         , "emailId": "abc@gmail.com"
//         , "gender": "Male"
//         , "dateOfBirth": "Wed Jun 27 2018 15:59:38 GMT+0530"
//         , "address": {
//             "line1": "gudgaon"
//         }
//     }
// Response: {
//     "responseCode": 200
//     , "responseMessage": "User added"
// }
// 2. http: //localhost:8084/user/login | POST
//     Request: {
//         "phoneNumber": 1234567867
//     }
// Response: {
//     "responseCode": 200
//     , "responseMessage": "Login successful"
//     , "result": {
//         "authToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjMzNmFhZDY3YzZmNDMwMTcyZWI5NWUiLCJzdGF0dXMiOiJBY3RpdmUiLCJjcmVhdGVkQXQiOiIyMDE4LTA2LTI3VDEwOjQ1OjAxLjU3OVoiLCJpYXQiOjE1MzAwOTY5NTN9.A97t04t0xbT4088m6aMnPFI-oho7Fpinjv6KqU9vsuQ"
//     }
// }
// 3. http: //localhost:8084/user/updateProfile | POST
//     Header: authtoken(required)
// Request: {
//     "name": "susheelll"
//     , "gender": "Male"
//     , "dateOfBirth": "Wed Jun 27 2018 15:59:38 GMT+0530"
//     , "address": {
//         "line1": "gudgaon"
//     }
// }
// Response: {
//     "responseCode": 200
//     , "responseMessage": "Details updated"
// }
// 4. http: //localhost:8084/user/deleteUser | POST
//     Header: authtoken(required)
// Resonse: {
//     "responseCode": 200
//     , "responseMessage": "User deleted"
// }
// 5. http: //localhost:8084/user/userList?searchBy=ab | GET
//     Header: authtoken(required)
// Response: {
//     "responseCode": 200
//     , "responseMessage": "User list"
//     , "result": [
//         {
//             "_id": "5b33749d7f595c362f1e121d"
//             , "updatedAt": "2018-06-27T11:27:25.554Z"
//             , "createdAt": "2018-06-27T11:27:25.554Z"
//             , "name": "abcd"
//             , "phoneNumber": 2323233456
//             , "emailId": "ab123@gmai2l21.com"
//             , "gender": "Male"
//             , "dateOfBirth": "2018-06-27T10:29:38.000Z"
//             , "__v": 0
//             , "status": "Active"
//             , "address": {
//                 "line1": "gudgaon"
//             }
//         }
//         , {
//             "_id": "5b3374aa7f595c362f1e121e"
//             , "updatedAt": "2018-06-27T11:27:38.347Z"
//             , "createdAt": "2018-06-27T11:27:38.347Z"
//             , "name": "abdef"
//             , "phoneNumber": 4323233456
//             , "emailId": "ab1223@gmai2l21.com"
//             , "gender": "Male"
//             , "dateOfBirth": "2018-06-27T10:29:38.000Z"
//             , "__v": 0
//             , "status": "Active"
//             , "address": {
//                 "line1": "gudgaon"
//             }
//         }
//     ]
// }