// helpers functions  ============
var sendResponse = require("../helpers/responseHandler");
var generateAuthToken = require("../helpers/generateAuthToken");
// Require mongodb schema==========
var user = require("../models/user");


var service = {};
service.registration = registration;
service.login = login;
service.updateProfile = updateProfile;
service.deleteUser = deleteUser;
service.userList = userList;

module.exports = service;



//@ user registraion api  | POST
//@ req.body { name , phoneNumber, emailId , gender, dateOfBirth, address}
// mandatory fields { phoneNumber,emailId,name }
function registration(req, res) {
	var condition = {
		$and: [{
			$or: [{
				phoneNumber: req.body.phoneNumber
			}, {
				emailId: req.body.emailId
			}]
		}, {
			status: "Active"
		}]
	};
	user.findOne(condition, (err, success) => {
		if (err) {
			sendResponse.withoutData(res, 500, "Server error");
		}
		else if (success) {
			if (success.emailId == req.body.emailId) {
				sendResponse.withoutData(res, 204, "Email id alrady exist");
			}
			else {
				sendResponse.withoutData(res, 204, "Phone number already exist");
			}
		}
		else {
			var userData = new user(req.body);
			userData.save((err, success) => {
				sendResponse.toUser(res, err, success, false, "User added", "Something went wrong");
			});
		}
	});
}
//@ login api | post
//@ body { phoneNumber }
function login(req, res) {
	var condition = {
		phoneNumber: req.body.phoneNumber
		, status: "Active"
	};
	user.findOne(condition).select("createdAt status").exec((err, success) => {
		if (err) {
			sendResponse.withoutData(res, 500, "Server error");
		}
		else if (success) {
			var token = generateAuthToken.loginUser(success);
			sendResponse.withData(res, 200, "Login successful", {
				authToken: token
			});
		}
		else {
			sendResponse.withoutData(res, 404, "Please enter correct phone number");
		}
	});
}
//@ updateProfile api | post
//@ get auth token in header 
//@ update body data
function updateProfile(req, res) {
	user.findByIdAndUpdate(req.decoded._id, req.body, (err, success) => {
		sendResponse.toUser(res, err, success, false, "Details updated", "Something went wrong");
	});
}
//@ deleteUser | POST
//@ only authtoken required in header
function deleteUser(req, res) {
	user.findByIdAndUpdate(req.decoded._id, {
		status: "Deleted"
	}, (err, success) => {
		sendResponse.toUser(res, err, success, false, "User deleted", "Something went wrong");
	});
}
//@ userList | Get
//@ get all user list without login user
//@ serach by name if name exist 
function userList(req, res) {
	var condition = {
		status: "Active"
	};
	if (req.query.searchBy) {
		condition["name"] = {
			$regex: ".*" + req.query.searchBy + ".*"
			, $options: "si"
		};
	}
	user.find(condition, (err, success) => {
		sendResponse.toUser(res, err, success, true, "User list", "List empty");
	});
}