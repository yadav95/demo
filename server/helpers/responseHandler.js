var sendResponseObj = {};

sendResponseObj.withoutData = withoutData;
sendResponseObj.withData = withData;
sendResponseObj.toUser = toUser;

module.exports = sendResponseObj;

function withoutData(res, responseCode, responseMessage) {
	// body...
	res.send({
		responseCode: responseCode,
		responseMessage: responseMessage
	});
}

function withData(res, responseCode, responseMessage, result) {
	// body...
	res.send({
		responseCode: responseCode,
		responseMessage: responseMessage,
		result: result
	});
}

function toUser(res, err, success, sendResponse, resMsg, otherMsg) {
	// body...
	if (err) {
		withoutData(res,500,"Server Error");
	} else if (success) {
		if (Array.isArray(success)) {
			if (success.length && sendResponse) {
				withData(res, 200, resMsg, success);
			} else {
				withoutData(res,404,otherMsg);
			}
		} else {
			if (sendResponse) {
				withData(res, 200, resMsg, success);
			} else {
				withoutData(res,200,resMsg);
			}
		}
	} else {
		withoutData(res,404,otherMsg);
	}
}