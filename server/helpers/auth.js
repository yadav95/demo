let jwt = require("jsonwebtoken");
let config = require("./config")();

//======== Require mongodb schema ========//
let user = require("../models/user");
module.exports.authenticate = function (req, res, next) {
	// Get authorization token in header
	let token = req.headers.authtoken;
	// if token exist then verify the token and authenticate 
	if (token) {
		jwt.verify(token, config.secretKey, function (err, decoded) {
			if (err) {
				return res.send({
					responseCode: 403
					, responseMessage: "Failed to authenticate token."
				});
			}
			else {
				req.decoded = decoded;             
				console.log("/------decoded token--------/");
				console.log(req.decoded);

				user.findById(req.decoded._id).exec((err, successData) => {
					if (err) {
						return res.send({
							responseCode: 500
							, responseMessage: "Server Error."
						});
					}
					else if (!successData) {
						return res.send({
							// data not found 
							responseCode: 403
							, responseMessage: "Invalid auth token"
						});
					}
					else {
						if (successData.status == "Active") {
							next();
						}
						else {
							// if status not equal to active
							return res.send({
								responseCode: 403
								, responseMessage: "Something went wrong"
							});
						}
					}
				});
			}
		});
	}
	else {
		// if token not exist then return to client token not provided
		return res.send({
			responseCode: 403
			, responseMessage: "no token provided"
		});
	}
};