const { check } = require("express-validator/check");

let name=[ check("name","name is required").exists()];
let phoneNumber = [ 
	check("phoneNumber","Phone number is required").exists(),
	check("phoneNumber","Phone number should be an integer").isInt(),
	check("phoneNumber", "Phone number must be 10 digit").isLength({ min: 10,max:10 })
];
let emailId = [
	check("emailId", "email required").exists(),
	check("emailId", "Invalid email address").isEmail()
];


var validateObj = {};

validateObj.registrationReq = [...name,...phoneNumber,...emailId];
validateObj.loginReq = [...phoneNumber];

module.exports = validateObj;




