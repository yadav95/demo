var jwt = require("jsonwebtoken");
var config = require("./config")();
var authToken = {
	"loginUser": function (data) {
		var token = jwt.sign({
			_id: data._id
			, status: data.status
			, createdAt : data.createdAt
		}, config.secretKey);
		return token;
	}
};
module.exports = authToken;