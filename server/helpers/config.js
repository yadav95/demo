module.exports = function () {
	var env = "devEnv";
	var devEnv = {
		PORT: 8084
		, DB_URL: "mongodb://localhost/demo"
		, secretKey:"123456@#$%"
	};
	var prodEnv = {
		PORT: 8084
		, DB_URL: "mongodb://localhost/demo"
		, secretKey:"123456@#$%"
	};
	return env == "devEnv" ? devEnv : prodEnv;
};