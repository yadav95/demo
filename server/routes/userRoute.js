// Require the npm package ============
let express = require("express");
let router = express.Router();
let validate = require("../helpers/validation");
const { validationResult } = require("express-validator/check");

//===== r helper function ========//
let auth = require("../helpers/auth");

// import  api services ======
let user = require("../services/user");




function sendResonse(res,msg){
	res.send({ responseCode: 422, responseMessage: msg});
}

function checkValidationResult(req, res, next){
	var result = validationResult(req).array();
	result.length ? sendResonse(res,result[0].msg) : next();
}

 


router.post("/registration", validate.registrationReq, (req, res, next) => { 
	 checkValidationResult(req, res, next);
}, user.registration); 

router.post("/login", validate.loginReq, (req, res, next) => { 
	 checkValidationResult(req, res, next);
}, user.login);  

router.post("/updateProfile", auth.authenticate ,user.updateProfile);  

router.post("/deleteUser", auth.authenticate ,user.deleteUser);

router.get("/userList", auth.authenticate ,user.userList);  




module.exports = router; 