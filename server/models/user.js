"use strict";

var mongoose = require("mongoose"),
	Schema = mongoose.Schema,
	userSchema = new Schema({
		name: {
			type: String
		},
		emailId: {
			type: String,
		},
		phoneNumber: {
			type: Number
		},
		gender: {
			type: String
		},
		dateOfBirth: {
			type: Date
		},
		address: {
			line1: {
				type: String,
				trim: true
			},
			line2: {
				type: String,
				trim: true
			},
			zipCode: {
				type: String
			},
			cityId: {
				type: Schema.Types.ObjectId,
			},
			stateId: {
				type: Schema.Types.ObjectId,
			}
		},
		status: {
			type: String,
			default: "Active"
		}
	}, {
		timestamps: true
	});

var user = mongoose.model("user", userSchema, "user");

module.exports = user;

